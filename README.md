## GCP Project directory

# # Workload identity federation

# What is Workload identity federation
Workload identity federation is a Keyless Authentication for service accounts

The benefits of using workload identity is
- Storage of access keys
- Distribution as needed
- Rotation of access keys using short-lived tokens ( security risk using service account keys)

So the problem this solves is the secret key management problem

# Where can we use Workload Identity Federation

- Any Provider that supports OIDC 
- For workloads in other cloud environments like AWS, Azure, Okta, Kubernetes clusters etc

For this to work, you need to create a Workload Identity Pool in GCP and
set IAM permissions for it

## Important Youtube videos
Use this youtube videos for learning Workload Identity and service account

- https://www.youtube.com/watch?v=XcKT_0kFqBw&list=PLpYLTz23z2abo3zsZ0ndhsu4Q9_Pm74_y&index=30&t=734s
- https://www.youtube.com/watch?v=-cp7GeQE2H0&t=69s


# GCP IAM

- https://cloud.google.com/iam/docs/workload-identity-federation
- https://cloud.google.com/iam/docs/workload-identity-federation#oidc
- https://cloud.google.com/iam/docs/workload-identity-federation#oidc-provider

So the Identity and Access Management in GCP has 3 main parts
 WHO -> CAN DO WHAT -> ON WHICH RESOURCE

 # WHO ( IDENTITY )
 - Service Account
 - User
 - Group
 - Google Group
 - Google Workspace User
 - Google Workspace Group
example: 
- Service Account: gitlab-ci-sa@softcomweb-project.iam.gserviceaccount.com
- Google Account: gitlab-ci-user@gmail.com
- Group: gitlab-ci-group
- Google Group: gitlab-ci-google-group
- Google Workspace User: gitlab-ci-google-workspace-user
- Google Workspace Group: gitlab-ci-google-workspace-group

# CAN DO WHAT ( ACCESS )
- Role
    - Premitive
        - Owner
        - Editor
        - Viewer
    - Predefined
        - Cloud Run Admin
        - Cloud Run Service Agent
        - Cloud Run Service Account
        - Cloud Run Service Account User
    - Custom
- Role Binding
    - Role Binding
    - Role Binding
    - Role Binding

# ON WHICH RESOURCE ( MANAGEMENT )
- Project
- Bucket
- Service Account
- Cloud Run Service
- Cloud Compute Instance
- Cloud Function
- Cloud Storage Bucket
- Cloud SQL Instance
- Cloud SQL Database
- Cloud SQL User
- Cloud SQL Database Instance

Resources: Organization -> Folder -> Project -> Service Account -> Role -> Role Binding -> Resource


# GCP IAM Policy

an example of a policy is

NOTE: Policies are primary made up of bindings. Roles are group of granular permissions.
```sample_policy.json
{
  "bindings": [
    {
      "role": "roles/storage.admin",
      "members": [
        "user:jane@example.com",
        "group:admins@example.com"
      ]
    },
    {
      "role": "roles/storage.viewer",
      "members": [
        "user:jane@example.com"
      ]
    }
  ],
  "etag": "BwWKmjvelug="
  "version": 1
}

```

So you will notice that the policy only defines who can do what. It does not define on which resource.
To define on which resource, you need to create an attachment.