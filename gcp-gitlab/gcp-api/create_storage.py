def run_create_storage():
    from google.cloud import storage

    storage_client = storage.Client()

    bucket_name = "softcomweb-project-api-storage-bucket-delete-me"

    #Create a new Bucket
    bucket = storage_client.create_bucket(bucket_name)

    print(f"[INFO] Bucket Output: {bucket}.")

    print(f"Bucket {bucket_name} created.")

if __name__ == "__main__":
    run_create_storage()
