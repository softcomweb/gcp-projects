data "tls_certificate" "gitlab" {
  url = "${var.gitlab_url}/oauth/discovery/keys"
}

resource "aws_iam_openid_connect_provider" "oidc_provider" {
  count = var.create_oidc_provider ? 1 : 0

  client_id_list  = [var.gitlab_url]
  thumbprint_list = [data.tls_certificate.gitlab.certificates[0].sha1_fingerprint]
  url             = var.gitlab_url
}

resource "aws_iam_role" "oidc_role" {
  count                = var.create_oidc_provider && var.create_oidc_role ? 1 : 0
  name                 = var.oidc_role_name
  description          = var.oidc_role_description
  max_session_duration = 3600
  assume_role_policy   = join("", data.aws_iam_policy_document.this[*].json)
  tags                 = var.tags
  inline_policy {
    name   = "gitlab-oidc-policy"
    policy = data.aws_iam_policy_document.gitlab_oidc_policy.json
  }

  depends_on = [aws_iam_openid_connect_provider.oidc_provider]
}

data "aws_iam_policy_document" "gitlab_oidc_policy" {
  statement {
    effect    = "Allow"
    actions   = ["sts:AssumeRole"]
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:CompleteLayerUpload",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetDownloadUrlForLayer",
      "ecr:InitiateLayerUpload",
      "ecr:ListImages",
      "ecr:PutImage",
      "ecr:UploadLayerPart",
    ]
    resources = ["*"]
  }
}

# data "gitlab_project" "projects" {
#   for_each = var.create_oidc_provider && var.create_oidc_role ? var.gitlab_projects : []
#   id       = each.key
# }

# data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "this" {

  dynamic "statement" {
    for_each = aws_iam_openid_connect_provider.oidc_provider

    content {
      actions = ["sts:AssumeRoleWithWebIdentity"]
      effect  = "Allow"

      condition {
        test     = "StringLike"
        values   = ["project_path:test:ref_type:branch:ref:*"]
        variable = "${join("", aws_iam_openid_connect_provider.oidc_provider[*].url)}:sub"
      }

      principals {
        identifiers = [statement.value.arn]
        type        = "Federated"
      }
    }
  }
  dynamic "statement" {
    for_each = aws_iam_openid_connect_provider.oidc_provider

    content {
      actions = ["sts:AssumeRole", "sts:TagSession"]
      effect  = "Allow"

      condition {
        test = "ArnLike"
        values = [
          "arn:aws:iam::11122211122211122:role/aws-reserved/sso.amazonaws.com/eu-central-1/AWSReservedSSO_AdministratorAccess_*",
          "arn:aws:iam::11122211122211122:role/aws-reserved/sso.amazonaws.com/eu-central-1/AWSReservedSSO_AWSAdministratorAccess_*"
        ]
        variable = "aws:PrincipalARN"
      }

      principals {
        identifiers = ["arn:aws:iam::11122211122211122:root"]
        type        = "AWS"
      }
    }
  }
}
